package telekosmos.udemy

import telekosmos.udemy.util.BFSData
import telekosmos.udemy.util.{VisitedState, NonVisited, ToVisit, Visited}

object FixtureGraph {
  /*

     2 ----- 7
   /  \     /
  1    4 - 6
  |\  /
  | 3 - 5
  |     |
  | --- 8

  */
  val nodes: Array[Int] = Array(1, 2, 3, 4, 5, 6, 7, 8)
  val startNode = 1
  val connections = Map(
    1 -> Array(2, 3, 8),
    2 -> Array(1, 4, 7),
    3 -> Array(1, 4, 5),
    4 -> Array(2, 3, 6),
    5 -> Array(3, 8),
    6 -> Array(4, 7),
    7 -> Array(2, 6),
    8 -> Array(1, 5)
  )

  def createMockRDD(): Array[(Int, BFSData)] = {
    for {
      node <- nodes
    } yield {
      // val state:VisitedState = if (node == startNode) ToVisit else NonVisited
      // val distance = if (node == startNode) 0 else Int.MaxValue

      (node, BFSData(connections(node), 0, NonVisited))
    }
  }
}

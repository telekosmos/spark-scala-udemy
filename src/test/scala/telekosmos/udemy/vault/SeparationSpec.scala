package telekosmos.udemy.vault

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import telekosmos.udemy.vault.Separation._

// import telekosmos.udemy.Separation._
import telekosmos.udemy.util.SharedSparkContext

/*

   2 ----- 7
 /  \     /
1    4 - 6
|\  /
| 3 - 5
|     |
| --- 8

*/
object TestGraph {
  val nodes: Array[Int] = Array(1, 2, 3, 4, 5, 6, 7, 8)
  val startNode = 1
  val connections = Map(
    1 -> Array(2, 3, 8),
    2 -> Array(1,4, 7),
    3 -> Array(1,4,5),
    4 -> Array(2,3,6),
    5 -> Array(3, 8),
    6 -> Array(4, 7),
    7 -> Array(2, 6),
    8 -> Array(1, 5)
  )

  def createMockRDD() = {
    for {
      node <- nodes
    } yield {
      val state = if (node == startNode) ToVisit else NonVisited
      val distance = if (node == startNode) 0 else Int.MaxValue

      (node, BFSData(connections(node), distance, state))
    }
  }
}


class SeparationSpec extends AnyFlatSpec with should.Matchers with SharedSparkContext {

  import Separation._
  import TestGraph._

  "mapDistances function" should "return a node for each connection" in {
    // map involves the infamous targetId -> this has to be parametrized/mocked
    Separation.startId = 3221
    val itemId = 3287
    val connections = Array(5946, 1077, 1602, 859, 2664, 6316)
    val distance = 0
    val state = ToVisit
    val inputNode = (itemId, BFSData(connections, distance, state))

    val res = Separation.mapDistances(startId, itemId)(inputNode)
    // expectations
    res should have length connections.length + 1

    // check res contains inputNode with state = visited
    res should contain (itemId, BFSData(connections, distance, Visited))
    val newOnes = res.filter(n => n._1 != itemId).filter(n => connections.contains(n._1))
    newOnes should have length connections.length
    newOnes.map(n => n._2.extract._1) should contain only Array()
    newOnes.map(n => n._2.extract._2) should contain only 1
    newOnes.map(n => n._2.extract._3) should contain only ToVisit
  }

  "reduce function" should "concatenate all neighbours" in {
    val accData = BFSData(Array(5946, 1077, 1602, 859, 2664, 6316), 0, ToVisit)
    val valData = BFSData(Array(298, 9902), 0, ToVisit)
    val expectedResult = Array(5946, 1077, 1602, 859, 2664, 6316, 298, 9902)

    val (res, _, _) = reduceNeighs(accData, valData).extract

    res shouldBe expectedResult
  }

  "reduce function" should "return minimun distance" in {
    val accData = BFSData(Array(), 1, ToVisit)
    val valData = BFSData(Array(), 4, ToVisit)
    val expectedResult = 1

    val (_, distance, _) = reduceNeighs(accData, valData).extract

    distance shouldBe expectedResult
  }

  "reduce function" should "return visited if any acc or val is visited" in {
    val accData = BFSData(Array(), 1, Visited)
    val valData = BFSData(Array(), 4, ToVisit)
    val expectedResult = Visited

    val (_, _, state) = reduceNeighs(accData, valData).extract

    state shouldBe expectedResult
  }

  "reduce function" should "return tovisit if no visited anywhere" in {
    val accData = BFSData(Array(), 1, ToVisit)
    val valData = BFSData(Array(), 4, NonVisited)
    val expectedResult = ToVisit

    val (_, _, state) = reduceNeighs(accData, valData).extract

    state shouldBe expectedResult
  }

  "reduce function" should "preserve distance and state for root node" in  {
    val rootNodeId = 3287
    val connections:Array[Int] = Array(5946, 1077, 1602, 859, 2664, 6316)
    val distance = 0
    val state = Visited

    val rootNodeData = BFSData(connections, distance, state)
    val dataFromNeighbour1 = BFSData(Array(), distance+3, ToVisit)
    val dataFromNeighbour2 = BFSData(Array(), distance+3, ToVisit)
    val dataFromNeighbour3 = BFSData(Array(), distance+3, Visited)

    val mapResult = Array(rootNodeData, dataFromNeighbour1, dataFromNeighbour2, dataFromNeighbour3)
    val reduceResult = mapResult.reduce(Separation.reduceNeighs)

    reduceResult shouldBe a [BFSData]
    reduceResult.extract._2 shouldBe 0
    reduceResult.extract._3 shouldBe Visited
  }

  "reduce function" should "preserve state in a 2 node graph" in {
    // 3287 5946
    // 5946 3287
    // ^^ the graph
    val rootNodeId = 3287
    val neighNodeId = 5946
    val rootConnections = Array(neighNodeId)
    val neighConnections = Array(rootNodeId)
    val distance = 0
    val rootState = ToVisit

    val rootNodeData:BFSData = BFSData(rootConnections, distance, rootState)
    val neighNodeData = BFSData(neighConnections, Int.MaxValue, NonVisited)
    val mappedNodeData = BFSData(Array(), distance+1, ToVisit)

    val mockRdd = Array((rootNodeId, rootNodeData), (neighNodeId, neighNodeData))
    val mockNodeData =  mockRdd.map(e => e._2)
    // BFSNode(neighNodeId, mappedNodeData)
    val mapResult:Array[(Int, BFSData)] = mockRdd.flatMap(mapDistances(0, 1))
    val mapIds:Map[Int, Array[(Int, BFSData)]] = mapResult.groupBy(_._1)
    val rootReduce = mapIds(rootNodeId).map(_._2).reduce(Separation.reduceNeighs)
    val neighReduce = mapIds(neighNodeId).map(_._2).reduce(Separation.reduceNeighs)
    // val reduceResult = mapResult.map(e => e._2).reduce(reduceNeighs)

    mapResult should have length 3
    (rootReduce.neighs, rootReduce.distance, rootReduce.state) should equal (rootConnections, 0, Visited)
    // (neighReduce._1, neighReduce._2, neighReduce._3) should equal ((neighConnections, 1, ToVisit))
    neighReduce.neighs should equal (neighConnections)
  }

  // val mapAndReduceByKey: Array[(Int, BFSData)] => Map[Int, BFSData]
  def withTestGraph(testCode: (Array[(Int, BFSData)], Array[(Int, BFSData)] => Map[Int, BFSData]) => Any): Unit = {
    val mockRDD = createMockRDD();
    val mapAndReduceByKey = (rdd:Array[(Int, BFSData)]) => {
      val map1 = rdd.flatMap(mapDistances(1, 4))
      val groupsByKey = map1.groupBy(_._1)
      val nodeDatas = for ((id, nodes) <- groupsByKey) yield (id, nodes.map((n) => n._2))
      val reduced = for ((id, bfsData) <- nodeDatas) yield (id, bfsData.reduce(reduceNeighs))

      reduced
    }
    try {
      testCode(mockRDD, mapAndReduceByKey)
    }
  }

  "fixture graph processing" should "be processed" in withTestGraph((mockRDD, mapAndReduceFn) => {
    mockRDD should have length(8)

    val iterations = Map(
      1 -> (Array(1,4,5), 1, ToVisit),
      2 -> (Array(1,4,5), 1, Visited),
      3 -> (Array(1,4,5), 1, Visited)
    )
    val checkNode3Expectations = (rdd:Map[Int, BFSData], i:Int) => {
      val (conns, distance, state) = rdd(3).extract
      val (expectedConns, expectedDist, expectedState) = iterations(i.min(3))
      conns should equal (expectedConns)
      distance should equal (expectedDist)
      state should === (expectedState)
    }

    var result = mapAndReduceFn(mockRDD)
    val toRdd = (reduceResult:Map[Int, BFSData]) => (for ((k, v) <- reduceResult) yield Tuple2(k, v)).toArray
    for (i <- 1 to 8) {
      checkNode3Expectations(result, i)
      result = mapAndReduceFn(toRdd(result))
    }
  })


  "graph processing" should "produce right distances" in withTestGraph((mockRDD, mapAndReduceFn) => {
    mockRDD should have length(8)

    var result = mapAndReduceFn(mockRDD)
    val toRdd = (reduceResult:Map[Int, BFSData]) => (for ((k, v) <- reduceResult) yield Tuple2(k, v)).toArray
    for (i <- 1 to 8) {
      result = mapAndReduceFn(toRdd(result))
    }

    val expectedDistances = Map(
      1 -> 0, 2 -> 1, 3 -> 1, 4 -> 2, 5 -> 2, 6 -> 3, 7 -> 2, 8 -> 1
    )
    result.size should be (8)
    val distances = toRdd(result).map(e => (e._1, e._2.extract._2)).toMap
    distances should equal(expectedDistances)
  })
}

package telekosmos.udemy

import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import telekosmos.udemy.util.{BFS, BFSData, NonVisited, SharedSparkContext, ToVisit, Visited}

class BFSFunctionsSpec extends AnyFlatSpec with BeforeAndAfterAll with Matchers with SharedSparkContext {

  var bfs:BFS = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    bfs = BFS()
  }

  def withGraph(testCode: Array[(Int, BFSData)] => Any) {
    val graphRdd = FixtureGraph.createMockRDD()
    try {
      testCode(graphRdd)
    }
  }

  "reduce function" should "concatenate all neighbours" in {
    val accData = BFSData(Array(5946, 1077, 1602, 859, 2664, 6316), 0, ToVisit)
    val valData = BFSData(Array(298, 9902), 0, ToVisit)
    val expectedResult = Array(5946, 1077, 1602, 859, 2664, 6316, 298, 9902)

    val (res, _, _) = BFS.reduceNeighs(accData, valData).extract

    res should equal(expectedResult)
  }

  "reduce function" should "return minimun distance" in {
    val accData = BFSData(Array(), 1, ToVisit)
    val valData = BFSData(Array(), 4, ToVisit)
    val expectedResult = 1

    val (_, distance, _) = BFS.reduceNeighs(accData, valData).extract

    distance should equal(expectedResult)
  }

  "reduce function" should "return visited if any acc or val is visited" in {
    val accData = BFSData(Array(), 1, Visited)
    val valData = BFSData(Array(), 4, ToVisit)
    val expectedResult = Visited

    val (_, _, state) = BFS.reduceNeighs(accData, valData).extract

    state should equal(expectedResult)
  }

  "reduce function" should "return tovisit if no visited anywhere" in {
    val accData = BFSData(Array(), 1, ToVisit)
    val valData = BFSData(Array(), 4, NonVisited)
    val expectedResult = ToVisit

    val (_, _, state) = BFS.reduceNeighs(accData, valData).extract

    state should equal(expectedResult)
  }

  "reduce function" should "preserve distance and state for root node" in  {
    val connections:Array[Int] = Array(5946, 1077, 1602, 859, 2664, 6316)
    val distance = 0
    val state = Visited

    val rootNodeData = BFSData(connections, distance, state)
    val dataFromNeighbour1 = BFSData(Array(), distance+3, ToVisit)
    val dataFromNeighbour2 = BFSData(Array(), distance+3, ToVisit)
    val dataFromNeighbour3 = BFSData(Array(), distance+3, Visited)

    val mapResult = Array(rootNodeData, dataFromNeighbour1, dataFromNeighbour2, dataFromNeighbour3)
    val reduceResult = mapResult.reduce(BFS.reduceNeighs)

    reduceResult shouldBe a [BFSData]
    reduceResult.extract._2 shouldBe 0
    reduceResult.extract._3 shouldBe Visited
  }
}

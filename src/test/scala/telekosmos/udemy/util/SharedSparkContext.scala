package telekosmos.udemy.util

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfterAll, Suite}

trait SharedSparkContext extends BeforeAndAfterAll { self: Suite =>
  @transient private var _sc: SparkContext = _

  def sc: SparkContext = _sc

  Logger.getLogger("telekosmos").setLevel(Level.ERROR)

  override def beforeAll(): Unit = {
    println("[SharedSparkContext] beforeAll")
    super.beforeAll()
    if (_sc == null) {
      val conf = new SparkConf().setAppName("Xeparation").setMaster("local[*]")
      _sc = SparkContext.getOrCreate(conf)
    }
  }

  override def afterAll(): Unit = {
    println("[SharedSparkContext] afterAll")
    try {
      _sc.stop()
      _sc = null
    }
    finally {
      super.afterAll()
    }
  }


}

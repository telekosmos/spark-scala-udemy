package telekosmos.udemy

import org.apache.spark.rdd.RDD
import org.scalatest.BeforeAndAfterAll
import org.scalatest.wordspec.AnyWordSpec
import telekosmos.udemy.util.{BFS, BFSData, SharedSparkContext}

class BFSSpec extends AnyWordSpec with BeforeAndAfterAll with SharedSparkContext {

  var bfs:BFS = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    bfs = BFS()
  }

  def withGraph(testCode: Array[(Int, BFSData)] => Any) {
    val graphRdd = FixtureGraph.createMockRDD()
    try {
      testCode(graphRdd)
    }
  }


  "Fake graph" should {
    "have distance 0 between same node" in withGraph { fakeRdd =>
      val rdd = sc.makeRDD(fakeRdd)
      val distance = bfs.separation(8)( 4, 4)(rdd)

      assert(distance === 0)
    }

    "have distance infinity if target is missing" in withGraph { fakeRdd =>
      val rdd = sc.makeRDD(fakeRdd)
      val distance = bfs.separation(8)( 1, 10)(rdd)

      assert(distance === Int.MaxValue)
    }

    "have distance 2 between 1 and 4" in withGraph( fakeRdd => {
      val rdd = sc.makeRDD(fakeRdd)
      val distance = bfs.separation(8)( 1, 4)(rdd)

      assert(distance === 2)
    })

    "have distance between 3 and 7" in withGraph( fakeRdd => {
      val rdd = sc.makeRDD(fakeRdd)
      val distance = bfs.separation(8)( 3, 7)(rdd)

      assert(distance === 3)
    })
  }

  def withTestFile(testCode: RDD[(Int, BFSData)] => Any): Unit = {
    try {
      val fileRDD = sc.textFile("../SparkScala/Marvel-graph.full.txt")
      val rdd = fileRDD.map(BFS.rowsToBFS)
      testCode(rdd)
    }
  }

  "Superhero file" should {
    "have distance between 5306, 14 is 2" in withTestFile { rdd =>
      val distance = bfs.separation(8)(5306, 14)(rdd)

      assert(distance === 2)
    }
  }
}

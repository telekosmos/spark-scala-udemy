package telekosmos.udemy

import collection.mutable.Stack
import org.scalatest._
import flatspec._
import matchers._


class GraphBFSSpec extends AnyFlatSpec with should.Matchers
  with BeforeAndAfter with BeforeAndAfterAll {

  import GraphBFS._

  "A Stack" should "pop values in last-in-first-out order" in {
    val stack = new Stack[Int]
    stack.push(1)
    stack.push(2)
    stack.pop() should be (2)
    stack.pop() should be (1)
  }

  it should "throw NoSuchElementException if an empty stack is popped" in {
    val emptyStack = new Stack[Int]
    a [NoSuchElementException] should be thrownBy {
      emptyStack.pop()
    }
  }

  "Distance in a single node graph" should "be zero" in {
    val g = Graph(List((5, List())))

    val ds = distances(g, 5)
    ds.size should be(1)
    ds should contain (5, 0)
  }

  "Distance from one node to the neighbour" should "be one" in {
    // (1)------(5)
    val g = Graph(List((5, List(1)), (1, List(5))))
    val ds = distances(g, 5)

    ds.size should be (2)
    ds should be (List((5, 0), (1, 1)))
  }

  "Distance" should "increase by one as more neighbours in between" in {
    val g = Graph(List(
      (5, List(2, 8)),
      (2, List(5)),
      (8, List(5, 2, 12)),
      (12, List(8))
    ))

    val ds5 = distances(g, 5)
    val ds2 = distances(g ,2)
    ds5.size should be (4)
    ds2.size should be (4)
    ds5 should contain allOf ((5, 0), (2, 1), (8, 1), (12, 2))
    ds2 should contain allOf ((2, 0), (5, 1), (8, 2), (12, 3))
  }
}

package telekosmos.udemy.util

import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator

import scala.collection.mutable.ArrayBuffer

abstract class VisitedState

case object NonVisited extends VisitedState

case object ToVisit extends VisitedState

case object Visited extends VisitedState

case class BFSData(neighs: Array[Int], distance: Int, state: VisitedState) extends Serializable {
  def extract: (Array[Int], Int, VisitedState) = (neighs, distance, state)
}

object BFS {
  def apply(): BFS = new BFS()

  def rowsToBFS(line: String): (Int, BFSData) = {
    val fields = line.split("\\s+")
    val heroID = fields(0).toInt

    var connections: ArrayBuffer[Int] = ArrayBuffer()
    for ( connection <- 1 until (fields.length - 1)) {
      connections += fields(connection).toInt
    }
    println(s"Processing $heroID with ${connections.size} connections")

    val state:VisitedState = NonVisited
    val distance:Int = Int.MaxValue

    (heroID, BFSData(connections.toArray, distance, state))
  }

  def init(graph: RDD[(Int, BFSData)], start:Int) = {
    val mapFn = (node:(Int, BFSData)) => {
      val (neighs, distance, _) = node._2.extract
      val id = node._1
      val (newDistance, newState) = if (id == start) (0, ToVisit) else (Int.MaxValue, NonVisited)

      (id, BFSData(neighs, newDistance, newState))
    }

    graph.map(mapFn)
  }

  def mapDistances(initialId: Int, endId: Int)(node: (Int, BFSData)): Array[(Int, BFSData)] = {
    val (itemId, (neighs, distance, state)) = (node._1, node._2.extract)
    var results: ArrayBuffer[(Int, BFSData)] = new ArrayBuffer[(Int, BFSData)]()
    var newState: VisitedState = state

    if (state == ToVisit) {
      neighs.foreach(n => {
        val (l, d, s) = (Array[Int](), distance + 1, ToVisit)
        results += ((n, BFSData(l, d, s)))
      })
      newState = Visited
    }

    // if (itemId == endId) longAccum.add(1)

    val thisNode = (itemId, BFSData(neighs, distance, newState))
    results += thisNode

    results.toArray
  }

  def reduceNeighs(acc: BFSData, value: BFSData): BFSData = {
    val (accNeighs, accDistance, accState) = acc.extract
    val (valNeighs, valDistance, valState) = value.extract

    val neighbours = accNeighs ++ valNeighs
    // there can be cycles, so...
    val distance = if (accDistance < valDistance) accDistance else valDistance
    val state = (accState, valState) match {
      case (NonVisited, NonVisited) => NonVisited
      case (NonVisited, ToVisit) => ToVisit
      case (ToVisit, NonVisited) => ToVisit
      case (ToVisit, ToVisit) => ToVisit
      case (ToVisit, Visited) => Visited
      case (Visited, ToVisit) => Visited
      case (Visited, Visited) => Visited
    }

    BFSData(neighbours, distance, state)
  }

}

// class instance is sent to the workers but contains something not serializable...
// most likely SparkContext
class BFS() {

  def separation(maxIterations: Int = -1)(start: Int, target: Int)(inputRDD: RDD[(Int, BFSData)]): Int = {
    var rdd = BFS.init(inputRDD, start)
    var i: Int = 0
    val iterations = if (maxIterations == -1) rdd.count() else maxIterations
    while (i < iterations) {
      println(s"@@@@@@@@@ Iteration $i")
      rdd = rdd.flatMap(BFS.mapDistances(start, target)).reduceByKey(BFS.reduceNeighs)
      i += 1
    }

    val result = rdd.collect().toMap
    println(s"@@@@@@@@@ $result")
    val (_, distance, _) = result.getOrElse(target, BFSData(Array(), Int.MaxValue, Visited)).extract

    distance
  }

}

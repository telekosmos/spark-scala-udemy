package telekosmos.udemy.vault

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.util.LongAccumulator

import scala.collection.mutable.ArrayBuffer

abstract class VisitedState
case object NonVisited extends VisitedState
case object ToVisit extends VisitedState
case object Visited extends VisitedState

object Separation {
  var startId:Int = 3287 // 6430 // 5306
  var targetId:Int = 14

  // val sc:SparkContext = new SparkContext("local[*]", "MySeparation")
  private val conf = new SparkConf().setAppName("NewSeparation").setMaster("local[*]")
  val sc:SparkContext = SparkContext.getOrCreate(conf)
  val longAccum = sc.longAccumulator("found")

  case class BFSData(neighs:Array[Int], distance:Int, state:VisitedState) {
    def extract: (Array[Int], Int, VisitedState) = (neighs, distance, state)
  }
  case class BFSNode(id:Int, data:BFSData) {
    def extract: (Int, BFSData) = (id, data)
  }

  def rowsToBFS(line: String): (Int, BFSData) = {
    val fields = line.split("\\s+")
    val heroID = fields(0).toInt

    var connections: ArrayBuffer[Int] = ArrayBuffer()
    for ( connection <- 1 until (fields.length - 1)) {
      connections += fields(connection).toInt
    }
    println(s"Processing $heroID with ${connections.size} connections")

    var state:VisitedState = NonVisited
    var distance:Int = Int.MaxValue

    // Unless this is the character we're starting from
    if (heroID == startId) {
      state = ToVisit
      distance = 0
    }

    (heroID, BFSData(connections.toArray, distance, state))
  }

  def createRDD (filename:String = "../SparkScala/Marvel-graph.full.txt") (sc: SparkContext): RDD[(Int, BFSData)] = {
    println(s"Creating RDD for file $filename")
    val inputFile = sc.textFile(filename)
    println(s"RDD created - ${inputFile.count()} elements")

    inputFile.map(rowsToBFS)
  }


  // def mapDistances(node: (Int, BFSData)): Array[(Int, BFSData)] = {
  def mapDistances(initialId:Int, endId:Int) (node: (Int, BFSData)): Array[(Int, BFSData)] = {
    val (itemId, (neighs, distance, state)) = (node._1, node._2.extract)
    var results:ArrayBuffer[(Int, BFSData)] = new ArrayBuffer[(Int, BFSData)]()
    var newState:VisitedState = state

    if (state == ToVisit) {
      neighs.foreach(n => {
        val (l, d, s) = (Array[Int](), distance+1, ToVisit)
        results += ((n, BFSData(l, d, s)))
      })
      newState = Visited
    }

    if (itemId == endId) longAccum.add(1)

    val thisNode = (itemId, BFSData(neighs, distance, newState))
    results += thisNode

    results.toArray
  }

  def reduceNeighs(acc:BFSData, value:BFSData): BFSData = {
    val (accNeighs, accDistance, accState) = acc.extract
    val (valNeighs, valDistance, valState) = value.extract

    val neighbours = accNeighs ++ valNeighs
    // there can be cycles, so...
    val distance = if (accDistance < valDistance) accDistance else valDistance
    val state = (accState, valState) match {
      case (NonVisited, NonVisited) => NonVisited
      case (NonVisited, ToVisit) => ToVisit
      case (ToVisit, NonVisited) => ToVisit
      case (ToVisit, ToVisit) => ToVisit
      case (ToVisit, Visited) => Visited
      case (Visited, ToVisit) => Visited
      case (Visited, Visited) => Visited
    }

    BFSData(neighbours, distance, state)
  }

  def separation(rddFactory: SparkContext => RDD[(Int, BFSData)])(maxSeparation:Int, start:Int, target:Int): Int = {
    var rdd = rddFactory(sc)

    var i:Int = 0
    while (i < maxSeparation && longAccum.value < 1) {
      // Have to get a new RDD on each iteration with distances and visited state updated
      // map rdd -> mapped = rdd.flatMap(mapDistances(longAccum))
      val mapped = rdd.flatMap(mapDistances(startId, targetId))
      if (longAccum.value == 1) {
        println(s"Found target $targetId which is $i separations from $startId")
      }
      else {
        rdd = mapped.reduceByKey(reduceNeighs)
        i += 1
      }
    }
    i
  }
  // val d = separation(createRDD())(15, 0, 0)

  def separation(maxIterations:Int, start:Int, target:Int)(inputRDD:RDD[(Int, BFSData)]): Int = {
    var rdd = inputRDD
    var i:Int = 0
    while (i < maxIterations && longAccum.value < 1) {
      // Have to get a new RDD on each iteration with distances and visited state updated
      // map rdd -> mapped = rdd.flatMap(mapDistances(longAccum))
      val mapped = rdd.flatMap(mapDistances(startId, targetId))
      if (longAccum.value == 1) {
        println(s"Found target $targetId which is $i separations from $startId")
      }
      else {
        rdd = mapped.reduceByKey(reduceNeighs)
        i += 1
      }
    }
    i
  }

  ////////////////////////////////////////////////////////////
  def distance(separation:Int = 15, start:Int, target:Int): Int = {
    Logger.getLogger("telekosmos").setLevel(Level.ERROR)

    var rdd = createRDD()(sc)
    var i:Int = 0
    while (i < separation && longAccum.value < 1) {
      // Have to get a new RDD on each iteration with distances and visited state updated
      // map rdd -> mapped = rdd.flatMap(mapDistances(longAccum))
      val mapped = rdd.flatMap(mapDistances(startId, targetId))
      if (longAccum.value == 1) {
        println(s"Found target $targetId which is $i separations from $startId")
      }
      else {
        rdd = mapped.reduceByKey(reduceNeighs)
        i += 1
      }
    }
    i
  }

  def main(args: Array[String]): Unit = {
    distance(start = 5306, target = 14)
    /*
    def outlog (prefix:String) (node: (Int, BFSData)) = {
      val id = node._1
      val data = node._2.extract
      println(s"$prefix ($id, (${data._1.mkString("Array(", ", ", ")")}, ${data._2}, ${data._3}))")
    }
    val rdd = createRDD(sc, "../SparkScala/Marvel-graph.txt")

    val logfn = outlog("@@@") _
    rdd
      .filter(e => e._2.extract._3 match {
        case ToVisit => true
        case _ => false
      })
      // .take(35)
      .collect()
      .foreach(logfn)
    */
  }
}

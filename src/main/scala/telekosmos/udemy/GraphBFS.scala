package telekosmos.udemy

import scala.collection.immutable.Queue
import scala.collection.mutable

object GraphBFS extends App {
  abstract class RecGraph
  case class Node[A](value: A, nodes: List[Node[A]]) extends RecGraph

  case class Graph[A](nodes:List[(A, List[A])])

  /*
  abstract class XTree[A]
  case object Leaf extends XTree
  case class Branch[A](value:A, left:XTree[A], right: XTree[A]) extends XTree[A]
  val t = new Branch(2, new Branch(1, Leaf, Leaf), Leaf)
  */

  def getNeighours(node:Int, g:Graph[Int]) = {
    g.nodes.find(p => p._1 == node)
  }

  def distances(g:Graph[Int], root:Int) = {
    val nodes:List[Int] = g.nodes.map(n => n._1)
    val seps = collection.mutable.Map[Int, Int]()
    nodes.foreach(n => seps(n) = Int.MaxValue)

    var distance = 0
    seps(root) = distance
    val q = new collection.mutable.Queue[Int]()
    q += root

    while (q.nonEmpty) {
      distance += 1
      val currentOnes = q.dequeueAll(_ => true)
      for (currentNode <- currentOnes) {
        g.nodes.find(p => p._1 == currentNode) match {
          case Some((_, neighs)) =>
            neighs.foreach(n => {
              if (seps(n) == Int.MaxValue) {
                q.enqueue(n)
                seps(n) = distance
              }
            })

          case None => ()
        }
      }
    }
    seps.toSeq
  }

  val g = Graph(List(
    (0, List(1, 2)),
    (1, List(0, 3)),
    (2, List(0, 3, 5)),
    (3, List(1, 4)),
    (4, List(3)),
    (5, List(2))
  ))


  val separations = distances(g, 0).sortBy(d => (d._2, d._1))
  println(s"Separations from 0 (node, degree): $separations")
}
